var express = require('express');
var router = express.Router();
var shared=  require('./shared');
var mongo=require('mongodb');
var objId=mongo.ObjectId;
router.post('/login-check',function(req,res){
     var user=req.body.data;
     console.log(user);
     shared.getMongoCon(res,function(db){
        var collection=db.collection('users');
        collection.find(user).toArray(function(e,r){
            if(e){
              console.log(e);
              res.send(e);
            }else{
              console.log(r);
              res.send(r);
            }
        })
     })


})

router.post('/reg-user',function(req,res){
     var userObj=req.body.user;

     shared.getMongoCon(res,function(db){
         var collection=db.collection('users');
         collection.insertOne(userObj,function(e,r){
           if(e){
             res.send(e);
           }else{
             res.send(r);
           }
         })
     })

})

router.get('/get-users',function(req,res){
      var role=req.query.role;

      var dataObj={
          role:role
      }

      shared.getMongoCon(res,function(db){
            var collection=db.collection('users');
            collection.find(dataObj).toArray(function(e,r){
                if(e){
                  res.send(e);
                }else{
                  res.send(r);
                }
            })
      })
})

router.get('/get-user-by-id',function(req,res){
      var id=req.query.id;
      var dataObj={
        _id:objId(id)
      }

      shared.getMongoCon(res,function(db){
        var collection=db.collection('users');
        collection.find(dataObj).toArray(function(e,r){
            if(e){
              res.send(e);
            }else{
              res.send(r);
            }
        })
  })

})

router.post('/update-vendor',function(req,res){
    var id=req.query.id;
    var data=req.body.data;

    var conObj={
      _id:objId(id)
    }
    
    shared.getMongoCon(res,function(db){
         var collection=db.collection('users');
         collection.updateOne(conObj,{$set:data},function(e,r){
           if(e){
             res.send(e);
           }else{
             res.send(r);
           }
         })
         
    })

    

})

router.get('/delete-user',function(req,res){
     var id=req.query.id;
     var deleteObj={
       _id:objId(id)
     }

     shared.getMongoCon(res,function(db){
            var collection=db.collection('users');
            collection.deleteOne(deleteObj,function(e,s){
              if(e){
                res.send(e);
              }else{
                res.send(s);
              }
            });
     })
})
   

router.post('/change-pwd',function(req,res){
     var id=req.body.id;
     var pwd=req.body.pwd;
     conObj={
       _id:objId(id)
     }
     data={
      'pwd':pwd
     }
     shared.getMongoCon(res,function(db){
      var collection=db.collection('users');
      collection.updateOne(conObj,{$set:data},function(e,r){
        if(e){
          res.send(e);
        }else{
          res.send(r);
        }
      })
    })
})

module.exports = router;
