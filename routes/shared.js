var mongodb=require('mongodb');

var shared={};

shared.getMongoCon=function(res,cb){
    var url="mongodb://localhost:27017";
    var mongoClient=mongodb.MongoClient;
    mongoClient.connect(url,function(err,cluster){
          if(err){
              res.send('db con error');
          }
          var db=cluster.db('onlineshopping');
          cb(db);
    })
}

module.exports=shared;